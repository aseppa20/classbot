/*
Name:         Brendan Golden
Date Created: 02/09/2022
 */

import { SlashCommandBuilder, SlashCommandSubcommandBuilder } from '@discordjs/builders';
import { CommandInteraction, Message } from "discord.js";
import { PG_Manager } from "../db";
import { Bap } from "../db_entities";

const command_start = new SlashCommandSubcommandBuilder().setName('start').setDescription('Start the Bap')
  .addBooleanOption(option => option.setName('silent').setDescription('Dont ping the user'));
const command_stop = new SlashCommandSubcommandBuilder().setName('stop').setDescription('Stop the Bap')
  .addBooleanOption(option => option.setName('silent').setDescription('Sont ping the user'));

const command = new SlashCommandBuilder().setName('bap').setDescription('Bap Hans')
    .addSubcommand(command_start)
    .addSubcommand(command_stop);

// you cannot run Hans!
const hans = "890608548449509376";

async function bap_start(db: PG_Manager, interaction: CommandInteraction) {
  if (interaction.user.id === hans) {
    return interaction.reply(`You have no power here <@${hans}>!  https://c.tenor.com/IS74NONBpy4AAAAd/abraxas-lotr.gif`);
  }

  const silent = interaction.options.getBoolean('silent');

  const to_bap = await db.get_items(Bap, { server: interaction.guildId, user: hans });
  if (to_bap.length === 0) {
    // need to create
    const bapping: Bap = {
      server: interaction.guildId,
      user: hans,
      bap: true,
      bap_current: 0,
      bap_max: 0,
      bap_total: 0,
    };
    await db.update_one(Bap, bapping);
  } else {
    // need to update
    // check if already active
    const bap_existing = to_bap[0];

    if (bap_existing.bap) {
      return interaction.reply({ content:`Already bapping <@${hans}>, ${bap_existing.bap_current} times this run!`, ephemeral: silent });
    } else {
      bap_existing.bap = true;
      bap_existing.bap_current = 0;
      await db.update_one(Bap, bap_existing);
    }
  }
  return interaction.reply({ content:`Now bapping <@${hans}>`, ephemeral: silent });
}

async function bap_stop(db: PG_Manager, interaction: CommandInteraction) {
  if (interaction.user.id === hans) {
    return interaction.reply(`You have no power here <@${hans}>!  https://c.tenor.com/IS74NONBpy4AAAAd/abraxas-lotr.gif`);
  }

  const silent = interaction.options.getBoolean('silent');

  const to_bap = await db.get_items(Bap, { server: interaction.guildId, user: hans });
  if (to_bap.length > 0) {
    // need to update
    // check if already active
    const bap_existing = to_bap[0];

    if (bap_existing.bap) {
      bap_existing.bap = false;
      await db.update_one(Bap, bap_existing);

      if (bap_existing.bap_current === bap_existing.bap_max) {
        // new max baps
        return interaction.reply({ content:`Stopping bapping <@${hans}>, ${bap_existing.bap_current} baps this run, a new record!. ${bap_existing.bap_total} baps total`, ephemeral: silent });
      } else {
        return interaction.reply({ content:`Stopping bapping <@${hans}>, ${bap_existing.bap_current} baps this run. ${bap_existing.bap_total} baps total`, ephemeral: silent });
      }
    }
  }

  return interaction.reply({ content: `Not bapping <@${hans}>, try \`\`/bap start\`\``, ephemeral: silent });
}

async function bap_message(db: PG_Manager, message: Message) {
    const to_bap = await db.get_items(Bap, { server: message.guildId, user: message.author.id, bap: true });
    if (to_bap.length === 0) {
      return;
    }

    await message.react('<a:bap:1015320926667153478>')
      .then(async () => {
        const bapping = to_bap[0];
        bapping.bap_current += 1;
        bapping.bap_total += 1;
        if (bapping.bap_current > bapping.bap_max) {
          bapping.bap_max = bapping.bap_current;
        }
        await db.update_one(Bap, bapping);
      }).catch(() => {
        console.log(`Failed to bap to "${message.content}" by <@${message.author.id}>`);
      });
}

module.exports = {
    data: command,

    // its named execute to standardise it across modules
    async execute(db: PG_Manager, interaction: CommandInteraction) {
        // split out by the subcommand
        switch (interaction.options.getSubcommand()) {
            case "start" : {
                return await bap_start(db, interaction);
            }
            case "stop" : {
                return await bap_stop(db, interaction);
            }
            default: {
                await interaction.reply('Please select a subcommand');
            }
        }
    },
    async message(db: PG_Manager, message: Message) {
        return await bap_message(db, message);
    }
    ,
};