import fs from 'fs';
import {
  ButtonInteraction,
  Client,
  Collection, CommandInteraction,
  Intents,
  Interaction, Message,
  MessageReaction,
  PartialMessageReaction,
  PartialUser,
  User,
} from "discord.js";
import { env, version, token, name, db_location } from "./config";
import { PG_Manager } from "./db";
import { Bap, Channels, Count } from "./db_entities";

interface Custom_Command {
  data: {
    name: string,
  },
  execute: (db: PG_Manager, interaction: CommandInteraction) => Promise<void>,
  setup?: (db: PG_Manager) => Promise<void>,
  reaction?: (db: PG_Manager, reaction: MessageReaction | PartialMessageReaction, user: User | PartialUser) => Promise<void>,
  buttons?: (db: PG_Manager, interaction: ButtonInteraction) => Promise<void>,
  message?: (db: PG_Manager, message: Message) => Promise<void>,
}
// this extends teh normal Clients object with another field.
// more info on Collections here: https://discordjs.guide/additional-info/collections.html#array-like-methods
interface Client_Custom extends Client{
    commands: Collection<string, Custom_Command>
}


main().catch((err) => console.log(err));
async function main() {
  // this measures how long it takes to get up and running
  const start = new Date().getTime();

  // bit of trickery to make .commands work in ts
  const client: Client_Custom = Object.assign(
    new Client({
      intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS],
      partials: ['MESSAGE', 'CHANNEL', 'REACTION'],
      presence: {
        activities: [
          {
            name: "your back",
            type: "WATCHING",
            url: "https://gitlab.com/c2842/misc/classbot",
          },
        ],
      },
    }),
    { commands: new Collection<string, Custom_Command>() }
  );

  const connection = new PG_Manager();
  await connection.up(db_location);

  // dynamically import the commands
  // the path here is relative to teh root of the project
  const commandFiles = fs.readdirSync('./build/src/commands').filter(file => file.endsWith('.js'));
  for (const file of commandFiles) {
    // the path here is relative to this file
    const command: Custom_Command = require(`./commands/${file}`);
    // Set a new item in the Collection
    // With the key as the command name and the value as the exported module

    // if it needs some setup, creating tabls etc then thenis is the spot
    if (command.setup) {
      await command.setup(connection);
    }

    client.commands.set(command.data.name, command);
  }

  client.once('ready', () => {console.log(`[${new Date().toISOString()}] [${env}] [v${version}] [${client.guilds.cache.size} guilds] [${(new Date().getTime() - start) / 1000}s startup for ${name}]`);});

  client.on('interactionCreate', async (interaction: Interaction) => manage_interactions(connection, client, interaction));
  client.on('messageReactionAdd', async (reaction, user) => manage_reactions(connection, client, reaction, user));
  client.on('messageReactionRemove', async (reaction, user) => manage_reactions(connection, client, reaction, user));
  client.on('messageCreate', async (message) => manage_messages(connection, client, message));

  await client.login(token);
}

/*
  This manages both slash commands and buttons.
  Sends the input off to teh relevent function
*/
async function manage_interactions(db: PG_Manager, client: Client_Custom, interaction: Interaction) {
  if (interaction.isCommand()) {
    const command = client.commands.get(interaction.commandName);
    if (!command) return;

    try {
      await command.execute(db, interaction);
    } catch (error) {
      console.error(error);
      await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
    }
  }
  if (interaction.isButton()) {
    const split = interaction.customId.split("_");
    if (split.length === 0) {
      return;
    }

    const command = client.commands.get(split[0]);
    if (!command) return;

    try {
      await command.buttons(db, interaction);
    } catch (error) {
      console.error(error);
      await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
    }
  }
}

/*
 This function listens for reactions.
 If it detects one it forwards it to the relevent sub command

 https://discordjs.guide/popular-topics/reactions.html#listening-for-reactions-on-old-messages
*/
async function manage_reactions(db: PG_Manager, client: Client_Custom, reaction?: MessageReaction | PartialMessageReaction, user?: User | PartialUser) {
  // When a reaction is received, check if the structure is partial
  if (reaction.partial) {
    // If the message this reaction belongs to was removed, the fetching might result in an API error which should be handled
    try {
      await reaction.fetch();
    } catch (error) {
      console.error('Something went wrong when fetching the message:', error);
      // Return as `reaction.message.author` may be undefined/null
      return;
    }
  }

  // fetch teh channel, if its not teh correct channel tehn ignore
  const is_assignments = await db.get_items(Channels, { server: reaction.message.guildId, command: "assignments" }, ["channel"]);
  if (is_assignments.length > 0) {
    const command = client.commands.get("assignment");
    if (!command) return;

    try {
      await command.reaction(db, reaction, user);
    }
    catch (error) {
      console.error('Something went wrong when processing the react:', error);
    }
    return;
  }
  // allow for other modules here using teh same method

}

/*
 This function handles messages.
*/
async function manage_messages(db: PG_Manager, client: Client_Custom, message: Message) {
  if (message.author.bot) {
    return;
  }
  // check if it is teh counting channel
  // if it is then send the message onto that function
  const is_count = await db.get_items(Count, { server: message.guildId, channel: message.channelId });
  if (is_count.length > 0) {
    const command = client.commands.get("count");
    if (!command) return;

    try {
      await command.message(db, message);
    }
    catch (error) {
      console.error('Something went wrong when processing the message:', error);
    }
    return;
  }
  const to_bap = await db.get_items(Bap, { server: message.guildId, user: message.author.id, bap: true });
  if (to_bap.length > 0) {
    const command = client.commands.get("bap");
    if (!command) return;

    try {
      await command.message(db, message);
    }
    catch (error) {
      console.error('Something went wrong when processing the message:', error);
    }
    return;
  }

}