{
  # from https://github.com/jakeisnt/react-turn-based-game/blob/1b5f333fe308ad2b90fdf5a5aa6fd9761d774297/flake.nix
  # https://github.com/soywod/react-pin-field/blob/master/flake.nix
  description = "Classbot";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-22.05";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils,  ... }: utils.lib.eachDefaultSystem (system: 
    let
    
      pkgs = import nixpkgs {
        inherit system;
        overlays = [  ];
      };
      
      package_name = "lm121_classbot";
      
      # set the node version here
      nodejs = pkgs.nodejs-16_x; 

    in rec {

  
      # https://gist.github.com/manveru/63131faddb6f21e2a7478b6fabcb978d
      # move teh output of mkYarnPackage insto somethign more owrkable
      # it was symlinking node_modules to a mostly empty folder
      # This moves everything into better places relative to each other 
      # As well as reducing the length of the path to the output
      
      
      # Sqlite3 does not like existing
      # https://github.com/NixOS/nixpkgs/blob/4b8488f72ac9f6cd964c83c2b1a9d273dc645b5b/pkgs/servers/web-apps/hedgedoc/default.nix
      packages."${package_name}" = pkgs.mkYarnPackage {
        name = "${package_name}";
        
        extraBuildInputs  = [
          pkgs.python3
        ];
        
        src = self;
        packageJSON = "${./package.json}";
        yarnLock = "${./yarn.lock}";
        
        dontInstall = true;
        
        postConfigure = ''
          rm deps/${package_name}/node_modules
          cp -R "$node_modules" deps/${package_name}
          chmod -R u+w deps/${package_name}
        '';

          buildPhase = ''
            runHook preBuild
            
            cd deps/${package_name}
            
            pushd node_modules/sqlite3
              export CPPFLAGS="-I${nodejs}/include/node"
              npm run install --build-from-source --nodedir=${nodejs}/include/node
            popd
            
            yarn build
            
            cd ../
            
            mkdir $out
            cp -R ${package_name}/. $out
            patchShebangs $out
            
            runHook postBuild
          '';

          # I just dont want it to run
          distPhase = ''
          
          '';
          
      };

      defaultPackage = packages."${package_name}";
      
      nixosModule = { lib, pkgs, config, ... }: 
        with lib; 
        let
          cfg = config.services."${package_name}";
        in { 
          options.services."${package_name}" = {
            enable = mkEnableOption "enable ${package_name}";
            
            config = mkOption rec {
              type = types.str;
              default = "./.env";
              example = default;
              description = "The config file";
            };
            
           # specific for teh program running
           prefix = mkOption rec {
              type = types.str;
              default = "silver_";
              example = default;
              description = "The prefix used to name service/folders";
           };
           
           user = mkOption rec {
              type = types.str;
              default = "${package_name}";
              example = default;
              description = "The user to run the service";
           };
           
           home = mkOption rec {
              type = types.str;
              default = "/etc/${cfg.prefix}${package_name}";
              example = default;
              description = "The home for the user";
           };
            
          };

          config = mkIf cfg.enable {
          
            users.groups."${cfg.user}" = { };
            
            users.users."${cfg.user}" = {
              createHome = true;
              isSystemUser = true;
              home = "${cfg.home}";
              group = "${cfg.user}";
            };
            
            systemd.services."${cfg.prefix}${cfg.user}" = {
              description = "Classbot";
              
              wantedBy = [ "multi-user.target" ];
              after = [ "network-online.target" ];
              wants = [ ];
              serviceConfig = {
                # fill figure this out in teh future
                #DynamicUser=true;
                User = "${cfg.user}";
                Group = "${cfg.user}";
                Restart = "always";
                WorkingDirectory="${self.defaultPackage."${system}"}";
                ExecStartPre = "${nodejs}/bin/node ./build/src/deploy-command.js";
                ExecStart = "${nodejs}/bin/node .";
                EnvironmentFile = "${cfg.config}";
              };
            };
            
          };
          
        };

      
    });
}